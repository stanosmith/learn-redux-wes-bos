import Raven from 'raven-js';

const sentry_key = '7cdaeccc7a02497da35c437056605884';
const sentry_app = '189155';
export const sentry_url = `https://${sentry_key}@sentry.io/${sentry_app}`;

export function logException(ex, context) {
  Raven.captureException(ex, {
    extra: context
  });
  /*eslint no-console:0*/
  window && window.console && console.error && console.error(ex);
}
